document.addEventListener('DOMContentLoaded', function() {
    let color_box = document.getElementById('color-box');
    let color_btn = document.getElementById('change-color-btn');
    function getRandomColor(){
        let red = Math.floor(Math.random() * 256);
        let green = Math.floor(Math.random() * 256);
        let blue = Math.floor(Math.random() * 256);

        let color = 'rgb(' + red + ',' + green + ',' + blue + ')';
        //console.log(color);
        color_box.style.backgroundColor = color;
    }
    
    color_btn.addEventListener('click', getRandomColor);
});